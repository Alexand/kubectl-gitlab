package main

import (
	"fmt"
	"os"

	"gitlab.com/hfyngvason/kubectl-gitlab/cmd"
	options "gitlab.com/hfyngvason/kubectl-gitlab/pkg/cmd"
	"k8s.io/cli-runtime/pkg/genericclioptions"
)

func main() {
	o := &options.GlobalOptions{
		KubeFlags: genericclioptions.NewConfigFlags(false),
	}
	rootCmd := cmd.NewRootCmd(o)
	rootCmd.AddCommand(cmd.NewAddCmd(o))
	rootCmd.AddCommand(cmd.NewDelCmd(o))
	rootCmd.AddCommand(cmd.NewListCmd(o))

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
