# kubectl-gitlab: A kubectl plugin to manage GitLab cluster associations

## Installation

### Pre-built binaries

1. Download the latest binary for your platform:
    - [macOS/amd64](https://gitlab.com/hfyngvason/kubectl-gitlab/-/jobs/artifacts/master/raw/bin/kubectl-gitlab-darwin-amd64?job=build%3A+%5Bdarwin%2C+amd64%5D)
    - [windows/amd64](https://gitlab.com/hfyngvason/kubectl-gitlab/-/jobs/artifacts/master/raw/bin/kubectl-gitlab-windows-amd64?job=build%3A+%5Bwindows%2C+amd64%5D)
    - [linux/amd64](https://gitlab.com/hfyngvason/kubectl-gitlab/-/jobs/artifacts/master/raw/bin/kubectl-gitlab-linux-amd64?job=build%3A+%5Blinux%2C+amd64%5D)

1. Rename it to `kubectl-gitlab` and move it to a folder in your `PATH` (such as `/usr/local/bin`) and make sure it is executable (`chmod +x kubectl-gitlab`)

1. On macOS, to run the binary for the first time, right-click and press `Open` and again `Open` in the subsequent dialog.

### Using go-get

```bash
GO111MODULE=on go get gitlab.com/hfyngvason/kubectl-gitlab
```

Make sure `$GOPATH/bin` is in your path.

### Cloning this repository

```bash
git clone https://gitlab.com/hfyngvason/kubectl-gitlab.git
cd kubectl-gitlab
go install
```

Again, make sure `$GOPATH/bin` is in your path.

## Usage

For more detailed usage instructions, run

```bash
kubectl gitlab help
```

In the below examples, `$project` and `$group` can be either IDs or paths.

### Add existing cluster to GitLab

```bash
kubectl gitlab --project $project add
kubectl gitlab --group $group add
kubectl gitlab --instance add
```

The add command also creates a service account `gitlab-admin` and a cluster role binding `gitlab-admin` in the `kube-system` namespace.

### List clusters associated with GitLab

```bash
kubectl gitlab --project $project list
kubectl gitlab --group $group list
kubectl gitlab --instance list
```

### Remove cluster association from GitLab

```bash
kubectl gitlab --project $project delete $cluster_id
kubectl gitlab --group $group delete $cluster_id
kubectl gitlab --instance delete $cluster_id
```

**Note:** This does not delete the service account and role binding created when the cluster was added. You can delete those separately via

```bash
kubectl -n kube-system delete clusterrolebinding gitlab-admin
kubectl -n kube-system delete serviceaccount gitlab-admin
```

## Configuration

### Authentication

A GitLab API token with the `api` scope is required, specified as either

- the environment variable `GITLAB_TOKEN`, or
- the flag `--gitlab-token`

### Custom base URL

By default, requests go to `https://gitlab.com`. You can customize this with

- the environment variable `GITLAB_URL`, or
- the flag `--gitlab-url`
