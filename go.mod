module gitlab.com/hfyngvason/kubectl-gitlab

go 1.14

require (
	github.com/pkg/errors v0.8.1
	github.com/spf13/cobra v1.0.0
	github.com/xanzy/go-gitlab v0.35.0
	k8s.io/api v0.18.6
	k8s.io/apimachinery v0.18.6
	k8s.io/cli-runtime v0.18.6
	k8s.io/client-go v0.18.6
)
