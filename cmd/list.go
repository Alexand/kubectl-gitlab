package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	entities "gitlab.com/hfyngvason/kubectl-gitlab/pkg"
	options "gitlab.com/hfyngvason/kubectl-gitlab/pkg/cmd"
)

// NewListCmd initializes a new "list" subcommand. It expects the GlobalOptions param to be initialized.
func NewListCmd(o *options.GlobalOptions) *cobra.Command {
	listCmd := &cobra.Command{
		Use:     "list [flags]",
		Aliases: []string{"ls"},
		Short:   "List project, group or instance clusters",
		RunE: func(cmd *cobra.Command, args []string) error {

			var clusterEntities []entities.Cluster

			if o.Project != "" {
				clusters, _, err := o.Gitlab.ProjectCluster.ListClusters(o.Project)
				if err != nil {
					return err
				}
				for _, cluster := range clusters {
					clusterEntities = append(clusterEntities, entities.FromProjectCluster(o, cluster))
				}
			} else if o.Group != "" {
				clusters, _, err := o.Gitlab.GroupCluster.ListClusters(o.Group)
				if err != nil {
					return err
				}
				for _, cluster := range clusters {
					clusterEntities = append(clusterEntities, entities.FromGroupCluster(o, cluster))
				}
			} else if o.Instance {
				clusters, _, err := o.Gitlab.InstanceCluster.ListClusters()
				if err != nil {
					return err
				}
				for _, cluster := range clusters {
					clusterEntities = append(clusterEntities, entities.FromInstanceCluster(o, cluster))
				}
			}
			result, err := json.MarshalIndent(clusterEntities, "", "    ")

			if err != nil {
				return err
			}

			fmt.Println(string(result))

			return nil
		},
	}
	return listCmd
}
