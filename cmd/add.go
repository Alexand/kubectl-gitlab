package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	entities "gitlab.com/hfyngvason/kubectl-gitlab/pkg"
	options "gitlab.com/hfyngvason/kubectl-gitlab/pkg/cmd"
	v1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth" // required for e.g. GCP, see https://github.com/kubernetes/client-go/issues/242
)

// NewAddCmd returns a new "add" subcommand. It expects the GlobalOptions param to be initialized
func NewAddCmd(o *options.GlobalOptions) *cobra.Command {
	addCmd := &cobra.Command{
		Use:   "add [flags]",
		Short: "Add cluster to GitLab",
		RunE: func(cmd *cobra.Command, args []string) error {
			ctx := context.Background()
			namespace := "kube-system"
			serviceAccountName := "gitlab"
			clusterRoleBindingName := "gitlab-admin"
			environmentScope, err := cmd.Flags().GetString("scope")
			if err != nil {
				return err
			}

			loader := o.KubeFlags.ToRawKubeConfigLoader()

			// get cluster name
			rawConfig, err := loader.RawConfig()
			if err != nil {
				return errors.Wrap(err, "Failed to load kubeconfig")
			}
			var gitlabClusterName string
			if *o.KubeFlags.ClusterName != "" {
				gitlabClusterName = *o.KubeFlags.ClusterName
			} else {
				currentContext, _ := rawConfig.Contexts[rawConfig.CurrentContext]
				// FIXME: Handle missing context
				gitlabClusterName = currentContext.Cluster
			}

			// init Kubernetes Clientset
			clientConfig, err := loader.ClientConfig()
			if err != nil {
				return errors.Wrap(err, "Failed to initialized kubernetes client config")
			}
			clientset, err := kubernetes.NewForConfig(clientConfig)
			if err != nil {
				return err
			}

			// find or create k8s resources
			gitlabAdminAccount := &v1.ServiceAccount{
				ObjectMeta: metav1.ObjectMeta{
					Name:      serviceAccountName,
					Namespace: namespace,
				},
			}
			_, err = clientset.CoreV1().ServiceAccounts(namespace).Create(
				ctx,
				gitlabAdminAccount,
				metav1.CreateOptions{},
			)
			if err != nil {
				return err
			}

			gitlabAdminClusterRoleBinding := &rbacv1.ClusterRoleBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name: clusterRoleBindingName,
				},
				RoleRef: rbacv1.RoleRef{
					APIGroup: "rbac.authorization.k8s.io",
					Kind:     "ClusterRole",
					Name:     "cluster-admin",
				},
				Subjects: []rbacv1.Subject{
					{
						Kind:      "ServiceAccount",
						Name:      serviceAccountName,
						Namespace: namespace,
					},
				},
			}
			_, err = clientset.RbacV1().ClusterRoleBindings().Create(
				ctx,
				gitlabAdminClusterRoleBinding,
				metav1.CreateOptions{},
			)
			if err != nil {
				return err
			}

			// get service account secret
			var secretName string
			for i := 0; i < 10 && secretName == ""; i++ {
				account, err := clientset.CoreV1().ServiceAccounts(namespace).Get(
					ctx, serviceAccountName, metav1.GetOptions{},
				)
				if err == nil && len(account.Secrets) > 0 {
					secretName = account.Secrets[0].Name
				}
				time.Sleep(time.Second)
			}
			if secretName == "" {
				return fmt.Errorf("timed out while waiting for service account secret")
			}
			secret, err := clientset.CoreV1().Secrets(namespace).Get(ctx, secretName, metav1.GetOptions{})
			if err != nil {
				return err
			}

			// add to GitLab
			token := string(secret.Data["token"])
			cert := string(secret.Data["ca.crt"])
			apiURL := clientConfig.Host + clientConfig.APIPath
			if o.Project != "" {
				gitlabCluster, _, err := o.Gitlab.ProjectCluster.AddCluster(
					o.Project,
					&gitlab.AddClusterOptions{
						Name:             &gitlabClusterName,
						EnvironmentScope: &environmentScope,
						PlatformKubernetes: &gitlab.AddPlatformKubernetesOptions{
							APIURL: &apiURL,
							Token:  &token,
							CaCert: &cert,
						},
					},
				)
				if err != nil {
					return err
				}
				clusterEntity := entities.FromProjectCluster(o, gitlabCluster)
				result, err := json.MarshalIndent(clusterEntity, "", "    ")
				if err != nil {
					return err
				}
				fmt.Println(string(result))
			} else if o.Group != "" {
				gitlabCluster, _, err := o.Gitlab.GroupCluster.AddCluster(
					o.Group,
					&gitlab.AddGroupClusterOptions{
						Name:             &gitlabClusterName,
						EnvironmentScope: &environmentScope,
						PlatformKubernetes: &gitlab.AddGroupPlatformKubernetesOptions{
							APIURL: &apiURL,
							Token:  &token,
							CaCert: &cert,
						},
					},
				)
				if err != nil {
					return err
				}
				clusterEntity := entities.FromGroupCluster(o, gitlabCluster)
				result, err := json.MarshalIndent(clusterEntity, "", "    ")
				if err != nil {
					return err
				}
				fmt.Println(string(result))
			} else if o.Instance {
				gitlabCluster, _, err := o.Gitlab.InstanceCluster.AddCluster(
					&gitlab.AddClusterOptions{
						Name:             &gitlabClusterName,
						EnvironmentScope: &environmentScope,
						PlatformKubernetes: &gitlab.AddPlatformKubernetesOptions{
							APIURL: &apiURL,
							Token:  &token,
							CaCert: &cert,
						},
					},
				)
				if err != nil {
					return err
				}
				clusterEntity := entities.FromInstanceCluster(o, gitlabCluster)
				result, err := json.MarshalIndent(clusterEntity, "", "    ")
				if err != nil {
					return err
				}
				fmt.Println(string(result))
			} else {
				return fmt.Errorf("Invalid cluster kind")
			}

			return nil
		},
	}
	o.KubeFlags.AddFlags(addCmd.Flags())
	addCmd.Flags().String("scope", "*", "GitLab environment scope for the cluster")
	return addCmd
}
